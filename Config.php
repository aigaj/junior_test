<?php
/**
 * Define properties for connecting to database
 */
return [
    'database' => [
        'name' => 'list_product',
        'username' => 'root',
        'password' => '',
        'connection' => 'mysql:host=localhost',
        'options' => [
            PDO::ERRMODE_EXCEPTION => PDO::ERRMODE_EXCEPTION
        ]
    ]
];



