<?php

namespace App\Core;

class App
{/**
 *
 * @var array
 */
    protected static $reqistry = [];

    /**
     *
     * @param string $key Assigns key to an array
     * @param string $value Assigns property to the key
     */
    public static function bind($key, $value)
    {
        static::$reqistry[$key] = $value;
    }

    /**
     * Check if registry contains the array
     * @return array
     * @throws Exception  If the key is not bound in the container
     */
    public static function get($key)
    {
        if (!array_key_exists($key, static::$reqistry)) {
            throw new Exception('No ($key) is bound in the container');
        }


        return static::$reqistry[$key];
    }
    
}