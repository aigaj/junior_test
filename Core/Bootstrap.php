<?php

use App\Core\App;

App::bind('config', require 'Config.php');

App::bind('database',
    new QueryBuilder(
    Connection::make(App::get('config')['database'])
));

/**
 *Renders view and provides product data
 *
 * @param string $name 
 * @param array $data 
 */

