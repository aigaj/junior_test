<?php

/**
 * Access to PDO object
 */
class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     *
     * @param array $table  All data from table in database.
     * @return string
     */
    public function selectAll($table)
    {

        $statement = $this->pdo->prepare("select * from ($table)");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Checks if check box is checked
     * Deletes all checked products
     * @param string $table
     */
    public function deleteSelected($table, $checkbox)
    {
        $statement = $this->pdo->prepare("DELETE  FROM $table WHERE id IN ("
            .implode(',', $checkbox).")");

        $statement->execute();
    }

    /**
     * Inserts selected data into selected table
     * Prepares a statement for execution and returns a statement object
     *
     * @param array $parameters
     * @param string $table
     *
     */
    public function insert($table, $parameters)
    {

        $sql = sprintf(
            'insert into %s (%s) values (%s)', $table,
            implode(',', array_keys($parameters)),
            ':'.implode(', :', array_keys($parameters))
        );

        try {

            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (Exception $e) {
            die('Something went wrong');
        }
    }
    
}