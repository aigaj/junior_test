<?php

namespace App\Controllers;

class Helper
{
    /*
     * Extracts data
     */
    public function view($name, $data = [])
    {
        extract($data);
        require "app/views/partials/header.php";
        require "app/views/{$name}_view.php";
    }

    /**
     *
     * @param string $path Returns URL.
     */
    public function redirect($path)
    {
        header("Location:/JuniorTest/{$path}");
    }
}