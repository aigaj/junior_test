<?php

namespace App\Controllers;

use App\Core\App;

/**
 * Class renders any views not containing anything dynamic
 */
class IndexController extends Controller
{

    /**
     *
     * @return string
     */
    public function home()
    {
        return $this->helper->view('index');
    }
}