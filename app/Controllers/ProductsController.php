<?php

namespace App\Controllers;

use App\Core\App;

/**
 * Class contains returned product data
 */
class ProductsController extends Controller
{

    /**
     * Contains products from database
     * @return string
     */
    public function index()
    {
        $products = $this->productsmodel->productList();
        return $this->helper->view('products', compact('products'));
    }

    /**
     * Method executed if submit button is pressed in products_view 
     * 
     * @return string
     */
    public function deleting()
    {

        $checkbox  = $_POST['num'];
        $selection = $_POST['select'];
        if (isset($_POST['delete'])) {
            if ($selection == 'del' && !empty($checkbox)) {
                $products = $this->productsmodel->delete($checkbox);
            }
        }
        return $this->helper->redirect('products');
    }
}