<?php

namespace App\Controllers;

/**
 * Class responsible for data validation
 * views the completed fields and checks if data is valid
 * if field(-s) are not suitable with defined rules shows an error
 *
 */
class ValidationController extends Controller
{

    /**
     *Defines rules for fields
     *
     * @param array $data
     */
    public function rules($data)
    {
        foreach ($data as $key => $value) {

            foreach ($value as $rule => $error) {

                switch ($rule) {

                    case 'decimal':
                        if (!is_numeric($this->test($_POST[$key])) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;

                    case 'alphanumeric':
                        if (!ctype_alnum($this->test($_POST[$key])) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;
                    case 'symbols':
                        if (!$this->check($this->characters($_POST[$key])) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;

                    case 'between':
                        foreach ($error as $between => $minmax) {

                            switch ($between) {
                                case 'min':

                                    if (strlen($this->test($_POST[$key])) < $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;

                                case 'max':

                                    if (strlen($this->test($_POST[$key])) > $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;
                            }
                        }
                        break;
                    case 'is_select':

                        if ($_POST[$key] == '' || $_POST[$key] == 'switch') {
                            $this->message[$key] = $error;
                        }

                        break;
                }
            }
        }
    }

    /**
     * Defines forbidden characters
     *
     * @param  string $data
     */
    public function characters($data)
    {

        $data = htmlentities(preg_replace("/[^(,)(.)(')(-)(%)A-Za-z0-9]/", "",
                $data), ENT_SUBSTITUTE);
        
        return $data;
    }

    /**
     * Defines allowed letters and symbols
     * @string type $value
     * @return boolean
     */
    public function check($value)
    {
        if (preg_match("/^[a-z0-9 \-\.\,\:\-]+$/i", $value)) {
            return true;
        } else {
            return false;
        }
    }

    public function display($value)
    {
        if (isset($_POST)) {
            if (isset($this->message[$value])) {
                echo '<div class="ValidationErrors">'.$this->message[$value].'</div>';
            }
        }
    }

    /**
     *
     * Leaves field filled if validation error accrued
     * @param string $item
     * @return string
     */
    public static function get($item)
    {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } elseif (isset($_GET[$item])) {
            return $_GET[$item];
        }
    }
/*
 * Replaces trims spaces and slashes from inserted data
 */
    public function test($data)
    {
        $data = str_replace(" ", "", trim($data));
        $data = str_replace("/", "", $data);
        return $data;
    }
}