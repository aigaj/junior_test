<?php

namespace App\Models;

use App\Core\App;

class BookModel extends ProductsModel
{

    /**
     * Assigns properties for books
     * @param string $collection
     */
    function __construct($collection)
    {
        $this->SKU           = $collection->SKU;
        $this->id            = $collection->id;
        $this->name          = $collection->name;
        $this->price         = $collection->price;
        $this->type_switcher = $collection->type_switcher;
        $this->weight        = $collection->weight;
    }

    /**
     * Gets  attribute for selected product
     *
     * @return string
     */
    public function getAttr()
    {
        return $this->weight.'g';
    }
}