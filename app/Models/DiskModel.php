<?php

namespace App\Models;

use App\Core\App;

class DiskModel extends ProductsModel
{

    /**
     *
     * @param string $collection Assigns properties for disks
     */
    function __construct($collection)
    {
        $this->SKU           = $collection->SKU;
        $this->id            = $collection->id;
        $this->name          = $collection->name;
        $this->price         = $collection->price;
        $this->type_switcher = $collection->type_switcher;
        $this->size          = $collection->size;
    }

    /**
     * Gets  attribute for selected product
     *
     * @return string
     */
    public function getAttr()
    {
        return $this->size.'MB';
    }
}