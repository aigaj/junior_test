<?php

namespace App\Models;

use App\Core\App;

class FurnitureModel extends ProductsModel
{

    /**
     * Assigns properties for furniture
     * @param type $collection
     */
    public function __construct($collection = [])
    {

        $this->SKU           = $collection->SKU;
        $this->id            = $collection->id;
        $this->name          = $collection->name;
        $this->price         = $collection->price;
        $this->type_switcher = $collection->type_switcher;
        $this->height        = $collection->height;
        $this->width         = $collection->width;
        $this->lenght        = $collection->lenght;
    }

    /**
     * Gets  attribute for selected product
     *
     * @return string
     */
    public function getAttr()
    {

        return implode('x',
            [
            $this->width,
            $this->height,
            $this->lenght
        ]);
    }
    
}