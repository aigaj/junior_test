<?php

namespace App\Models;

use App\Core\App;



class ProductsModel
{
const TABLE = 'products';

    /**
     * Divides data by selected type_switcher.
     * Each data is being sorted by type (disk, book, furniture)
     * @return array
     */
    public function productList()
    {
        $products   = App::get('database')->selectAll(self::TABLE);
        $collection = [];
        foreach ($products as $product) {
            if ($product->type_switcher == 'disk') {
                $collection [] = new \App\Models\DiskModel($product);
            } elseif ($product->type_switcher == 'book') {
                $collection [] = new \App\Models\BookModel($product);
            } elseif ($product->type_switcher == 'furniture') {
                $collection[] = new \App\Models\FurnitureModel($product);
            }
        }
        return $collection;
    }

    /**
     * Redirect to QueryBuilder class to store data in table
     *
     * @param array $data
     *
     */
    public function storing($data)
    {
       
        App::get('database')->insert(self::TABLE, $data);
    }

    public function delete($checkbox)
    {
        App::get('database')->deleteSelected(self::TABLE, $checkbox);
    }

    /**
     *
     * @return string
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getSKU()
    {
        return $this->SKU;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price.'€';
    }
    
}