</header>
<body>
    <h1>Products List</h1> 

    <form method="POST" action="products"  class="products-form">
        <select name="select" >
            <option value="none" disabled selected>---------</option>
            <option value="del">Mass Delete</option>
        </select>
        <input type="submit" id="delete" name="delete" > 

        <?php
        //Give variable a name
        //Get specific attribute for each category from type switcher
        foreach ($products as $product):
            ?>

            <table><div>

                    <th><input type="checkbox"  name="num[]" class="delete_id" value="<?= $product->id; ?>"> </th>
                    <tr><td>SKU:</td>  <td><?= $product->getSKU(); ?> </td></tr>
                    <tr><td>Name:</td>  <td><?= $product->getName(); ?></td></tr>
                    <tr><td>Price:</td>  <td><?= $product->getPrice(); ?>  </td></tr>
                    <tr><td>Attribute:</td>  <td><?= $product->getAttr(); ?>  </td></tr>

                </div>
            <?php endforeach; ?>
    </form>
</body>
