<?php
require 'vendor/autoload.php';

require 'Core/Bootstrap.php';


use App\Core\{
    Router,
    Request
};

Router::load('app/Routes.php')
    ->direct(Request::uri(), Request::method());
